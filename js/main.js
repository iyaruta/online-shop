$(document).ready(function(){
    $('.gallery-slider-nav').slick({
        slidesToShow: 5,
        slidesToScroll: 1,
        dots: false,
        centerMode: true,
        focusOnSelect: true,
        arrow: true,
        asNavFor: '.gallery-slider'
    });
    $('.gallery-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        asNavFor: '.gallery-slider-nav',
        arrows: false,
        fade: false
    });

    $('#gallery-tabs li:eq(1) a').tab('show');
});